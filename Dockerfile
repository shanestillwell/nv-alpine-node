ARG NODE_VERSION=12-alpine
FROM node:${NODE_VERSION}
LABEL maintainer="shane@northernv.com"

RUN apk update && \
    apk upgrade && \
    apk add --no-cache curl jq bash git postgresql coreutils dumb-init

RUN npm install -g --unsafe-perm=true netlify-cli nodemon

EXPOSE 80 443
